/*
 * geda_pcb.h - Dump objects in the gEDA PCB board/module format
 *
 * Written 2009, 2011 by Werner Almesberger, 2016 by Erich Heinzle
 * Copyright 2009, 2011 by Werner Almesberger
 * Copyright 2016, Erich Heinzle
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef GEDA_H
#define GEDA_H

#include <stdio.h>


int geda(FILE *file, const char *one);

#endif /* !GEDA_H */
